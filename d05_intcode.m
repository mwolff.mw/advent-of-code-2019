function out = d05_intcode(x, input)

idx = 1;
input_idx = 1;
relative_base = 0;

while idx <= numel(x)
    instr = x(idx);
    opcode = round(100 * (instr * 0.01 - floor(instr * 0.01)));
    modes = pad(num2str(floor(instr * 0.01)), 3, 'left', '0');
    modes = fliplr(arrayfun(@str2double, modes));
    
    switch opcode
        case 1
            pos = x(idx+3) + 1;
            arg1 = getArg(modes(1), idx+1);
            arg2 = getArg(modes(2), idx+2);           
            val = arg1 + arg2;
            setValueAtPos(pos, val);
            idx = idx + 4;
        case 2
            pos = x(idx+3) + 1;
            arg1 = getArg(modes(1), (idx+1));
            arg2 = getArg(modes(2), (idx+2));     
            val = arg1 * arg2;
            setValueAtPos(pos, val);
            idx = idx + 4;
        case 3
            if modes(1) == 0
                pos = x(idx+1) + 1;
            elseif modes(1) == 2
%                 try
                    pos = relative_base + x(idx+1) + 1
%                 catch
%                     pos = 1;
%                 end
            end
            if nargin == 2
                val = input(input_idx);
                input_idx = input_idx + 1;
            else
                val = input('Value? ');
            end
            setValueAtPos(pos, val);
            idx = idx + 2;
        case 4
            val = getArg(modes(1), (idx+1));
            out = val;
            disp(out);
            idx = idx + 2;
        case 5
            arg1 = getArg(modes(1), (idx+1));
            arg2 = getArg(modes(2), (idx+2));
            if arg1 ~= 0
                idx = 1 + arg2;
            else
                idx = idx + 3;
            end
        case 6
            arg1 = getArg(modes(1), (idx+1));
            arg2 = getArg(modes(2), (idx+2));
            if arg1 == 0
                idx = 1 + arg2;
            else
                idx = idx + 3;
            end
        case 7
%             pos = x(idx+3) + 1;
            arg1 = getArg(modes(1), idx+1);
            arg2 = getArg(modes(2), idx+2);
            pos  = getArg(modes(3), idx+3) + 1;
            val = arg1 < arg2;
            setValueAtPos(pos, val);
            idx = idx + 4;
        case 8
            pos = x(idx+3) + 1;
            arg1 = getArg(modes(1), (idx+1));
            arg2 = getArg(modes(2), (idx+2));
            val = arg1 == arg2;
            setValueAtPos(pos, val);
            idx = idx + 4;
        case 9
            arg1 = getArg(modes(1), (idx+1));
            relative_base = relative_base + arg1;
            idx = idx + 2;
        otherwise
            return;
    end
end

    function setValueAtPos(pos, val)
        if pos <= numel(x)
            x(pos) = val;
        else
            x = [x, zeros(1, pos-numel(x))];
            x(pos) = val;
        end
    end


    function arg = getArg(mode, idx)
        try
        switch mode
            case 0; arg = x(x(idx)+1);
            case 1; arg = x(idx);
            case 2; arg = x(relative_base+x(idx)+1);
        end
        catch
            arg = 0;
        end
    end

end