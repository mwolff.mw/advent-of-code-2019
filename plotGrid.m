function plotGrid(grid, currentPosition)
X = repmat(1:size(grid,2),size(grid,1),1);
Y = repmat((size(grid,1):-1:1)',1,size(grid,2));
grid(currentPosition(1), currentPosition(2)) = 1.5*max(grid(:));
hold on;
pcolor(X, Y, grid)
ax = gca;
ax.XLim = [1 size(grid,2)];
ax.YLim = [1 size(grid,1)];
hold off;
drawnow;
end