function [output, program, position] = d09_intcode(program, input)

position = 0 : numel(program)-1;
output = [];
toSend = [];

currentPosition = 0;
currentPositionInput = 1;
relativeBase = 0;

interactiveMode = nargin == 2 && ischar(input) && strcmp(input, 'interactive');

%Create communication files for interactive mode
if interactiveMode
    cmIn  = TcpIpCommunicator(30000, true);
    cmOut = TcpIpCommunicator(30001, false);
end

while currentPosition <= max(position)
    %Get the instruction
    instr = program(position == currentPosition);
    %Compute the opcode
    opcode = round(100 * (instr * 0.01 - floor(instr * 0.01)));
    %Compute modes
    modes = pad(num2str(floor(instr * 0.01)), 3, 'left', '0');
    modes = fliplr(arrayfun(@str2double, modes));
    %Apply transform according to opcode value
    switch opcode
        case 1
            pos  = getPos(modes(3), currentPosition+3);
            arg1 = getArg(modes(1), currentPosition+1);
            arg2 = getArg(modes(2), currentPosition+2);
            val = arg1 + arg2;
            setValueAtPos(pos, val);
            currentPosition = currentPosition + 4;
        case 2
            pos  = getPos(modes(3), currentPosition+3);
            arg1 = getArg(modes(1), currentPosition+1);
            arg2 = getArg(modes(2), currentPosition+2);
            val = arg1 * arg2;
            setValueAtPos(pos, val);
            currentPosition = currentPosition + 4;
        case 3
            if interactiveMode
                if ~isempty(toSend); cmOut.write(toSend); end
                toSend = [];
                val = cmIn.getNextData();
            else
                val = input(currentPositionInput);
                currentPositionInput = currentPositionInput + 1;
            end
            pos = getPos(modes(1), currentPosition+1);
            setValueAtPos(pos, val);
            currentPosition = currentPosition + 2;
        case 4
            val = getArg(modes(1), currentPosition+1);
            output(end+1) = val;
            if interactiveMode
                toSend(end+1) = val;
            else
%                 disp(val);
            end
            currentPosition = currentPosition + 2;
        case 5
            arg1 = getArg(modes(1), currentPosition+1);
            arg2 = getArg(modes(2), currentPosition+2);
            if arg1 ~= 0
                currentPosition = arg2;
            else
                currentPosition = currentPosition + 3;
            end
        case 6
            arg1 = getArg(modes(1), currentPosition+1);
            arg2 = getArg(modes(2), currentPosition+2);
            if arg1 == 0
                currentPosition = arg2;
            else
                currentPosition = currentPosition + 3;
            end
        case 7
            pos  = getPos(modes(3), currentPosition+3);
            arg1 = getArg(modes(1), currentPosition+1);
            arg2 = getArg(modes(2), currentPosition+2);
            val = arg1 < arg2;
            setValueAtPos(pos, val);
            currentPosition = currentPosition + 4;
        case 8
            pos  = getPos(modes(3), currentPosition+3);
            arg1 = getArg(modes(1), currentPosition+1);
            arg2 = getArg(modes(2), currentPosition+2);
            val = arg1 == arg2;
            setValueAtPos(pos, val);
            currentPosition = currentPosition + 4;
        case 9
            arg1 = getArg(modes(1), currentPosition+1);
            relativeBase = relativeBase + arg1;
            currentPosition = currentPosition + 2;
        otherwise
            if interactiveMode
                if ~isempty(toSend); cmOut.write(toSend); end
                disp('Exiting');
            end
            return;
    end
end

    function setValueAtPos(pos, val)
        if ~isempty(find(position == pos, 1))
            program(position == pos) = val;
        else
            position(end+1) = pos;
            program(end+1) = val;
        end
    end

    function out = getPos(mode, pos)
        switch mode
            case 0; out = program(position == pos);
            case 2; out = relativeBase + program(position == pos);
            otherwise; error('Unsupported mode');
        end
    end


    function out = getArg(mode, pos)
        if pos < 0
            error('Position cannot be negative');
        end
        switch mode
            case 0; out = program(program(pos == position) == position);
            case 1; out = program(pos == position);
            case 2; out = program(relativeBase + program(pos == position) == position);
        end
        if isempty(out)
            out = 0;
        end
    end

end