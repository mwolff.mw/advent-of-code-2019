function sp = d18_computeShortestPath(grid, keysToCollect, keysCollected, sp)

if nargin == 2; keysCollected = ''; end

closedDoors = char(65:90);
closedDoors = closedDoors(~ismember(closedDoors, upper(keysCollected)));

if nargin == 2
    keysToCollect = ['@', keysToCollect];    
    pathStart = repmat(keysToCollect', numel(keysToCollect), 1);
    pathEnd = repelem(keysToCollect', numel(keysToCollect));
    
    numElm = numel(pathStart);
    distance = +Inf * ones(numElm, 1);
    itemsOnPath = repmat({''}, numElm, 1);
    isUsable = true(numElm, 1);
    
    for idx = 1:numElm
        
        [distance(idx), items] = ...
            shortestPathInGrid(grid,  ...
            gridtools.getPosition(grid, pathStart(idx)),  ...
            gridtools.getPosition(grid, pathEnd(idx)));
        
        items = strrep(items, '@', '');
        items = strrep(items, '.', '');
        
        itemsOnPath{idx} = items;
        isUsable(idx) = ~any(ismember(items, closedDoors)) && ~any(ismember(items, keysToCollect));
    end
    
    sp = table(pathStart, pathEnd, distance, itemsOnPath, isUsable);
   %Remove rows in excess
   sp = sp(sp.pathStart ~= sp.pathEnd & sp.pathEnd ~= '@', :);
   sp.itemsOnPath = char(sp.itemsOnPath);
    
else
    
%     sp = sp(~ismember(sp.pathEnd, keysCollected), :);
    sp.isUsable = ~any(ismember(sp.itemsOnPath, [closedDoors,keysToCollect]), 2);
    
%     sp.isUsable = cellfun(@(x) ~any(ismember(x, [closedDoors,keysToCollect])), sp.itemsOnPath);
%     for idx = 1:size(sp,1)
%         if ~sp.isUsable(idx)
%             sp.isUsable(idx) = ~any(ismember(sp.itemsOnPath{idx}, closedDoors)) && ~any(ismember(sp.itemsOnPath{idx}, keysToCollect));
%         end
%     end
    
    
end