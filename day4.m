function day4(x,y)

count = 0;
candidates = [];
for n = x : y
    str = num2str(n);
    test_adjacent = ... 
        str(1) == str(2) || ...
        str(2) == str(3) || ...
        str(3) == str(4) || ...
        str(4) == str(5) || ...
        str(5) == str(6);
    test_increase = ...
        str(1) <= str(2) && ...
        str(2) <= str(3) && ...
        str(3) <= str(4) && ...
        str(4) <= str(5) && ...
        str(5) <= str(6);
    if test_adjacent && test_increase
        count = count + 1;
        candidates(end+1) = n;
    end
end
count

count = 0;
for n = candidates
    str = num2str(n);
    test_adjacent = test_adjacent_2(str);
    test_increase = ...
        str(1) <= str(2) && ...
        str(2) <= str(3) && ...
        str(3) <= str(4) && ...
        str(4) <= str(5) && ...
        str(5) <= str(6);
    count = count + (test_adjacent && test_increase) ;   
end
count

end

function flag = test_adjacent_2(str)

flag = false;
for ii = 0 : 9
    mask = str == num2str(ii);
    idx = find(mask);
    if nnz(mask) == 2 && abs(idx(1)-idx(2)) == 1
        flag = true;
        return;
    end
end

    
end
