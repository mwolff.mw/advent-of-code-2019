function [numSteps, items] = shortestPathInGrid(grid, pos, target, makeItemsUnique)

%Here we assume the grid is built as follows
%Walls are marked by '#' signs
%Walkable areas are marked by '.' signs

numSteps = 0;
items = '';

if isequal(pos, target); return; end

%Grid values around current position
l = grid(pos(1), pos(2)-1);
r = grid(pos(1), pos(2)+1);
b = grid(pos(1)+1, pos(2));
t = grid(pos(1)-1, pos(2));


around = [l r b t];
isWalkable = around ~= '#';

%Check if there is a portal around
%The following variable is a 4-element vector that stores true/false flags 
%indicating is there is a portal on the left/right/bottom/top%
portal = repmat({}, 1, 4);
if pos(2) > 2; portal{1} = regexp(grid(pos(1), pos(2)-2:pos(2)-1), '[A-Z]{2}', 'match'); end
if pos(2) < size(grid,2)-1; portal{2} = regexp(grid(pos(1), pos(2)+1:pos(2)+2), '[A-Z]{2}', 'match'); end
if pos(1) < size(grid,1)-1; portal{3} = regexp(grid(pos(1)+1:pos(1)+2, pos(2))', '[A-Z]{2}', 'match'); end
if pos(1) > 2; portal{4} = regexp(grid(pos(1)-2:pos(1)-1, pos(2))', '[A-Z]{2}', 'match'); end

for idx = 1:4; if ~isempty(portal{idx}); portal{idx} = portal{idx}{1}; end; end
% for idx = 3:4; if ~isempty(portal{idx}); portal{idx} = portal{idx}'; end; end


portal{4}
return

if nnz(isWalkable) == 0
    numSteps = +Inf;
    return;
    
elseif nnz(isWalkable) == 1
    numSteps = numSteps + 1;
    items = [items, grid(pos(1), pos(2))];
    grid(pos(1), pos(2)) = '#';
    switch find(isWalkable)
        case 1; posNew = [pos(1), pos(2)-1];
        case 2; posNew = [pos(1), pos(2)+1];
        case 3; posNew = [pos(1)+1, pos(2)];
        case 4; posNew = [pos(1)-1, pos(2)];
    end
    [numStepsIncr, itemsIncr] = shortestPathInGrid(grid, posNew, target, false);
    numSteps = numSteps + numStepsIncr;
    items = [items, itemsIncr];
else
    numSteps = numSteps + 1;
    grid(pos(1), pos(2)) = '#';
    numStepsIncrByPath = +Inf * ones(1,numel(around));
    itemsIncrByPath = cell(1,numel(around));
    for idx = 1 : numel(numStepsIncrByPath)
        if isWalkable(idx)
            switch idx
                case 1; posNew = [pos(1), pos(2)-1];
                case 2; posNew = [pos(1), pos(2)+1];
                case 3; posNew = [pos(1)+1, pos(2)];
                case 4; posNew = [pos(1)-1, pos(2)];
            end
            [numStepsIncrByPath(idx), itemsIncrByPath{idx}] = shortestPathInGrid(grid, posNew, target, false);
        end
    end
    [~, idx] = min(numStepsIncrByPath);
    numSteps = numSteps + numStepsIncrByPath(idx);
    items = [items, itemsIncrByPath{idx}];
end

if nargin == 3 && makeItemsUnique
    items = unique(items);
end

end