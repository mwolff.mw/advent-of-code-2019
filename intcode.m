function x = intcode(x)

for idx = 1 : 4 : numel(x)
    op = x(idx);
    if op == 1
        pos = x(idx+3) + 1;
        x(pos) = x(x(idx+1)+1) + x(x(idx+2)+1);
    elseif op == 2
        pos = x(idx+3) + 1;
        x(pos) = x(x(idx+1)+1) * x(x(idx+2)+1);
    else
        return;
    end
end

