function out = d06_numOrbits(ol, or, obj)
%Compute the number of orbits for the input object
%ol: objects on the left side of orbital relationships
%or: objects on the right side of orbital relationships
%obj: object for which the number of orbits is to be computed

out = 0;
idx = find(strcmp(or, obj));
if ~isempty(idx)
    out = out + 1;
    out = out + d06_numOrbits(ol, or, ol{idx});
end