function out = d06_numOrbits(ol, or, obj)
%NUMORBITS Compute the number of orbits for the input object
%ol: objects on the left side of orbital relationships
%or: objects on the right side of orbital relationships
%obj: object for which the number of orbits is to be computed

out = 0;
if ismember(obj, or)
    out = out + 1;
%     idx = strcmp(or, obj);
    out = out + d06_numOrbits(ol, or, ol{strcmp(or, obj)});
else
    out = 0;
end
end