function out = day7_signal_with_feedback(puzzle_input, phase_setting_sequence)

signal = zeros(1,5);
firstTimeHere = true;

while true
for amp = 1 : 5
    input = [phase_setting_sequence(amp), 0];
    if firstTimeHere
        firstTimeHere = false;
    else
        input = [phase_setting_sequence(amp), signal(end)];
    end
    if amp > 1
        input(2) = signal(amp-1);
    end
    signal(amp) = intcode_day5(puzzle_input, input);
end
out = signal(end)
end

