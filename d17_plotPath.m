function d17_plotPath(grid, path)
direction = 'up';
[row, col] = ind2sub(size(grid), find(grid == '^'));
path = strsplit(path, ',');
for idx = 1 : 2 : numel(path)
    switch path{idx}
        case 'L'
            switch direction
                case 'up'; direction = 'left';
                case 'down'; direction = 'right';
                case 'left'; direction = 'down';
                case 'right'; direction = 'up';
            end
        case 'R'
            switch direction
                case 'up'; direction = 'right';
                case 'down'; direction = 'left';
                case 'left'; direction = 'up';
                case 'right'; direction = 'down';
            end
    end
    numMoves = str2double(path{idx+1});
    switch direction
        case 'up'; for n = 1:numMoves; row = row-1; grid(row, col) = 'P'; end
        case 'down'; for n = 1:numMoves; row = row+1; grid(row, col) = 'P'; end
        case 'left'; for n = 1:numMoves; col = col-1; grid(row, col) = 'P'; end
        case 'right'; for n = 1:numMoves; col = col+1; grid(row, col) = 'P'; end
    end
end

disp(grid);