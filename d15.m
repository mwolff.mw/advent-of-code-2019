grid = zeros(50);

% [grid, previous, corners] = exploreGrid(grid);
exploreGrid(grid)
function [grid, previous, corners] = exploreGrid(grid)

%Define standard values for plot colors
valueWall = 1;
valuePathToAvoid = 5;
valuePath = 8;

%Position the droid at the center of the grid
droid = floor(size(grid)/2);
grid(droid(1), droid(2)) = valuePath;

%Initialize communicators
cmIn  = TcpIpCommunicator(30000, false);
cmOut = TcpIpCommunicator(30001, true);

%Initialize some data structures
positions = {}; %previously visited positions
possibleMoves = {}; %possible moves at these positions
corners = {};
%
previous.position = {}; %path followed by the droid
previous.programInput = [];

unexplored.position = {};
unexplored.direction = [];

programInput = 0;
markPathToAvoid = false;
foundOxygenSystem = false;
stopExploring = false;
while ~stopExploring
    %     askAgain = true;
    %     while askAgain
    %         programInput = input('Direction: ');
    %         askAgain = isempty(programInput) || ~ismember(programInput, [1 2 3 4]);
    %     end
    
    plotGrid(grid, droid);
    
    %Try going in each direction to detect walls
    %Only perform this analysis if it has not been performed yet
    disp(['Current position: ', pos2str(droid)]);
    idxPosition = find(cellfun(@(x) isequal(x, droid), positions));
    if isempty(idxPosition)
        %Moves to analyze
        moves = [1,2,3,4];
        possibleMovesCurrent = moves;
        %Remove the reverse move of the last move
        moves = moves(moves ~= getReverseMove(programInput));
        for idx = 1 : numel(moves)
            programInputTest = moves(idx);
            if (programInputTest == 1 && grid(droid(1)-1, droid(2)) == valueWall) ...
                    || (programInputTest == 2 && grid(droid(1)+1, droid(2)) == valueWall) ...
                    || (programInputTest == 3 && grid(droid(1), droid(2)-1) == valueWall) ...
                    || (programInputTest == 4 && grid(droid(1), droid(2)+1) == valueWall)
                outputTest = 0;
            else
                cmIn.write(programInputTest);
                outputTest = cmOut.getNextData();
            end
            if outputTest == 0
                %Display a wall on the grid
                switch programInputTest
                    case 1; wall = [droid(1)-1, droid(2)];
                    case 2; wall = [droid(1)+1, droid(2)];
                    case 3; wall = [droid(1), droid(2)-1];
                    case 4; wall = [droid(1), droid(2)+1];
                end
                grid(wall(1), wall(2)) = valueWall;
                possibleMovesCurrent = possibleMovesCurrent(possibleMovesCurrent ~= moves(idx));
            else
                %Otherwise, do the reverse move
                programInputTest = getReverseMove(moves(idx));
                cmIn.write(programInputTest);
                cmOut.getNextData();
            end
        end
        %Save the position and the associated possible moves
        positions{end+1} = droid;
        possibleMoves{end+1} = possibleMovesCurrent;
    else
        possibleMovesCurrent = possibleMoves{idxPosition};
    end
    
    %Now do the next move
    if numel(possibleMovesCurrent) == 1
        %If the droid is stuck, mark the cell as a cell to avoid and move
        %to the only possible direction
        disp('Droid is stuck in a corner!');
        saveCornerPosition(droid);
        grid(droid(1), droid(2)) = valuePathToAvoid;
        if numel(positions) > 1; markPathToAvoid = true; end
        %Move to the only possible position
        programInput = possibleMoves{cellfun(@(x) isequal(x, droid), positions)};
        output = moveDroid(programInput);
        %Indicate this path should be avoided until the next intersection
    else
        if markPathToAvoid && numel(possibleMovesCurrent) == 3
            grid(droid(1), droid(2)) = valuePath;
            markPathToAvoid = false;
        end
        %If there are more than one possible positions, analyze the grid
        gridValuesAround = arrayfun(@getGridValue, possibleMovesCurrent);
        %If the droid is in a situation where all possible locations have
        %already been explored, move to a previous unexplored location
        if numel(possibleMovesCurrent) == 3 && ~any(gridValuesAround == 0)
            if numel(unexplored.position) == 0
                stopExploring = true;
            else
                plotGrid(grid, droid);
                goTo = unexplored.position{1};
                moveTo(goTo);
            end
        else
            %Remove moves to cells that should be avoided
            %Possibly move to a cell that has not been visited yet
            movesToKeep = gridValuesAround ~= valuePathToAvoid & gridValuesAround == 0;
            if ~any(movesToKeep)
                movesToKeep = gridValuesAround ~= valuePathToAvoid;
            end
            candidateMoves = possibleMovesCurrent(movesToKeep);
            %If there still are two possible moves, do not make the one that is
            %the opposite of the previous move
            if numel(candidateMoves) > 1
                candidateMoves = candidateMoves(candidateMoves ~= getReverseMove(programInput));
            end
            %Select randomly one of the remaining possible moves
            programInput = candidateMoves(randi(numel(candidateMoves)));
            %Is there an unexplored path that the droid won't visit now?
            moveToUnexplored = possibleMovesCurrent(gridValuesAround == 0 & possibleMovesCurrent ~= programInput);
            if any(moveToUnexplored)
                %Make sure the unexplored path is not yet in the list of
                %unexplored paths
                mask = cellfun(@(x) isequal(x, droid), unexplored.position) & unexplored.direction == moveToUnexplored;
                if ~any(mask)
                    unexplored.position{end+1} = droid;
                    unexplored.direction(end+1) = moveToUnexplored;
                    disp(['Unexplored direction(s): ', num2str(moveToUnexplored)]);
                end
            end
            %Move the droid
            output = moveDroid(programInput);
        end
    end
    foundOxygenSystem = output == 2;
    if foundOxygenSystem
        disp(['Found oxygen system at position ', pos2str(droid)]);
%         stopExploring = true;
    end
end

    function output = moveDroid(programInput)
        previous.position{end+1} = droid;
        previous.programInput(end+1) = programInput;
        %If the droid is about to explore a path left unexplored
        %previously, remove it from the list of unexplored directions
        toRemove = cellfun(@(x) isequal(x, droid), unexplored.position) & unexplored.direction == programInput;
        if any(toRemove)
            disp(['Removing position ', pos2str(droid), ' from the list of unexplored paths, ' ...
                'now ', num2str(numel(unexplored.position)-1), ' places left to explore']);
            unexplored.position = unexplored.position(~toRemove);
            unexplored.direction = unexplored.direction(~toRemove);
        end
        %Now move the droid
        cmIn.write(programInput);
        output = cmOut.getNextData();
        if output == 1 || output == 2
            switch programInput
                case 1; droid(1) = droid(1) - 1;
                case 2; droid(1) = droid(1) + 1;
                case 3; droid(2) = droid(2) - 1;
                case 4; droid(2) = droid(2) + 1;
            end
        end
        if markPathToAvoid
            grid(droid(1), droid(2)) = valuePathToAvoid;
        else
            grid(droid(1), droid(2)) = valuePath;
        end
    end

    function saveCornerPosition(pos)
        if ~any(cellfun(@(x) isequal(x, pos), corners))
            corners{end+1} = pos;
        end
    end

    function val = getGridValue(programInput)
        switch programInput
            case 1; val = grid(droid(1)-1, droid(2));
            case 2; val = grid(droid(1)+1, droid(2));
            case 3; val = grid(droid(1), droid(2)-1);
            case 4; val = grid(droid(1), droid(2)+1);
        end
    end

    function moveTo(pos)
        disp(['Now moving from ', pos2str(droid), ' to ', pos2str(pos)]);
        %Look for a shorter path
        idxDroid  = find(cellfun(@(x) isequal(x, droid), previous.position));
        idxTarget = find(cellfun(@(x) isequal(x, pos), previous.position));
        idxDiff = idxDroid - idxTarget';
        idxDiffIsPositive = idxDiff > 0;
        idxDiff = idxDiff .* idxDiffIsPositive;
        idxDiff(idxDiff == 0) = +Inf;
        [~, idxMinDiff] = min(idxDiff(:));
        [idxTargetMin, idxDroidMin] = ind2sub(size(idxDiff), idxMinDiff);
        idxDroid = idxDroid(idxDroidMin);
        
        idxPos = numel(previous.position);
        while ~isequal(droid, pos)
            moveDroid(getReverseMove(previous.programInput(idxPos)));
            idxPos = idxPos-1;
        end
    end


end

function str = pos2str(position)
str = ['x=', num2str(position(1)), ' y=', num2str(position(2))];
end

function moveOut = getReverseMove(moveIn)
switch moveIn
    case 0; moveOut = 0;
    case 1; moveOut = 2;
    case 2; moveOut = 1;
    case 3; moveOut = 4;
    case 4; moveOut = 3;
end
end

