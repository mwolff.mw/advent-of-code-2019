classdef TcpIpCommunicator < handle
    %FILEBASEDCOMMUNICATOR Implementation of a communicator that uses text
    %files as communication protocol
    
    properties (Access = private)
        conn;
    end
   
    methods
        function obj = TcpIpCommunicator(port, isServer)
            if isServer
                obj.conn = tcpip('0.0.0.0', port, 'NetworkRole', 'server');
            else
                obj.conn = tcpip('localhost', port, 'NetworkRole', 'client');
            end
            obj.conn.OutputBufferSize = 1e6;
            obj.conn.InputBufferSize = 1e6;
            fopen(obj.conn);
        end
        
        function data = read(obj)
            data = [];
            while obj.conn.BytesAvailable > 0
                data = [data; fread(obj.conn, obj.conn.BytesAvailable)];
            end
        end
        
        function data = getNextData(obj)
            while obj.conn.BytesAvailable == 0
                pause(0.01);
            end
            data = obj.read();
        end
        
        function write(obj, data)
            fwrite(obj.conn, data);
        end
    end
end

