function out = intcode_day5(x, input)

idx = 1;
input_idx = 1;
relative_base = 0;
while idx <= numel(x)
    instr = x(idx);
    opcode = round(100 * (instr * 0.01 - floor(instr * 0.01)));
%     modes = flipud(cellfun(@(x) ~isempty(x) && strcmp(x, '1'), cellstr(pad(num2str(floor(instr * 0.01)), 3, 'left')')));
    modes = pad(num2str(floor(instr * 0.01)), 3, 'left', '0');
    modes = fliplr(arrayfun(@str2double, modes));
        
    switch opcode
        case 1
            pos = x(idx+3) + 1;
            arg1 = get_arg(modes(1), idx+1);
            arg2 = get_arg(modes(2), idx+2);           
            val = arg1 + arg2;
            setValueAtPos(pos, val);
            idx = idx + 4;
        case 2
            pos = x(idx+3) + 1;
            arg1 = get_arg(modes(1), (idx+1));
            arg2 = get_arg(modes(2), (idx+2));     
            val = arg1 * arg2;
            setValueAtPos(pos, val);
            idx = idx + 4;
        case 3
            pos = x(idx+1) + 1;
            if nargin == 2
                val = input(input_idx);
                input_idx = input_idx + 1;
            else
                val = input('Value? ');
            end
            setValueAtPos(pos, val);
            idx = idx + 2;
        case 4
            val = get_arg(modes(1), (idx+1));
            out = val;
            disp(out);
            idx = idx + 2;
        case 5
            arg1 = get_arg(modes(1), (idx+1));
            arg2 = get_arg(modes(2), (idx+2));
            if arg1 ~= 0
                idx = 1 + arg2;
            else
                idx = idx + 3;
            end
        case 6
            arg1 = get_arg(modes(1), (idx+1));
            arg2 = get_arg(modes(2), (idx+2));
            if arg1 == 0
                idx = 1 + arg2;
            else
                idx = idx + 3;
            end
        case 7
            pos = x(idx+3) + 1;
            arg1 = get_arg(modes(1), (idx+1));
            arg2 = get_arg(modes(2), (idx+2));
            val = arg1 < arg2;
            setValueAtPos(pos, val);
            idx = idx + 4;
        case 8
            pos = x(idx+3) + 1;
            arg1 = get_arg(modes(1), (idx+1));
            arg2 = get_arg(modes(2), (idx+2));
            val = arg1 == arg2;
            setValueAtPos(pos, val);
            idx = idx + 4;
        case 9
            arg1 = get_arg(modes(1), (idx+1));
            relative_base = relative_base + arg1;
            idx = idx + 2;
        otherwise
            return;
    end
end

    function setValueAtPos(pos, val)
        if pos <= numel(x)
            x(pos) = val;
        else
            x = [x, zeros(1, pos-numel(x))];
            x(pos) = val;
        end
    end


    function arg = get_arg(mode, idx)
        try
        switch mode
            case 0; arg = x(x(idx)+1);
            case 1; arg = x(idx);
            case 2; arg = x(relative_base+x(idx)+1);
        end
        catch
            arg = 0;
        end
    end


end