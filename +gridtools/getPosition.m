function pos = getPosition(grid, character)
[row, col] = ind2sub(size(grid), find(grid == character));
pos = [row, col];
end