function [objects, pathLength] = d06_objectsOrbitingAround(ol, or, obj)
%Get the list of objects the input object is orbiting around
%ol: objects on the left side of orbital relationships
%or: objects on the right side of orbital relationships
%obj: object for which the number of orbits is to be computed

objects = {};
pathLength = [];
if ismember(obj, or)
    objLeft = ol{strcmp(or, obj)};
    objects{end+1} = objLeft;
    pathLength(end+1) = 1;
    [objectsInner, pathLengthInner] = d06_objectsOrbitingAround(ol, or, objLeft);
    for idx = 1 : numel(objectsInner)
        objects{end+1} = objectsInner{idx};
        pathLength(end+1) = pathLengthInner(idx) + 1;
    end
end