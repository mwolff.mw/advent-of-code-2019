y1 = strsplit(x1, ',');
y2 = strsplit(x2, ',');

[m1, ~] = wirepath(y1);
[m2, ~] = wirepath(y2);

[ii,jj] = ind2sub(size(m), find(m1 + m2 > 1));
distance = abs(ii-start(1)) + abs(jj-start(2));
mask = distance > 0;
ii = ii(mask);
jj = jj(mask);
distance = distance(mask);
min(distance)

n1 = zeros(1, numel(distance));
n2 = zeros(1, numel(distance));
for idx = 1 : numel(distance)
    target = [ii(idx), jj(idx)];
    [~, n1(idx)] = wirepath(y1, target);
    [~, n2(idx)] = wirepath(y2, target);
end
n = n1 + n2
min(n)


function [m, num_steps] = wirepath(x, target)

sz = 50000;
m = sparse(sz, sz);
start = floor(size(m)/2);
m(start(1), start(2)) = 1;
num_steps = 0;
look_for_target = nargin == 2;

pos = start;
for  idx = 1 : numel(x)
    elm = x{idx};
    direction = elm(1);
    distance = str2double(elm(2:end));
    for d = 1 : distance
        if direction == 'R'
            pos(2) = pos(2) + 1;
        elseif direction == 'U'
            pos(1) = pos(1) - 1;
        elseif direction == 'L'
            pos(2) = pos(2) - 1;
        else
            pos(1) = pos(1) + 1;
        end
        num_steps = num_steps + 1;
        if look_for_target && isequal(pos, target); return; end
        m(pos(1),pos(2)) = 1;
    end
end
end