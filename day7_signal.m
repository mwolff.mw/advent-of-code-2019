function out = day7_signal(puzzle_input, phase_setting_sequence)
signal = zeros(1,5);
for amp = 1 : 5
    input = [phase_setting_sequence(amp), 0];
    if amp > 1
        input(2) = signal(amp-1);
    end
    signal(amp) = intcode_day5(puzzle_input, input);
end
out = signal(end);
% out = sum(signal .* 10 .^ (4:-1:0));