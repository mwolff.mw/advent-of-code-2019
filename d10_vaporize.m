function [angle,vaporized] = d10_vaporize(im, x, y)
%D10_VAPORIZE Summary of this function goes here
%   Detailed explanation goes here


numVaporizedSoFar = 0;
vaporized = zeros(size(im));
while nnz(im == '#') > 1
    %Get the map of remaining asteroids
    [~, asteroidMap] = d10_numAsteroids(im, x, y);
    %Go through the list of asteroids, compute the angle
    angle = ones(size(asteroidMap)) * 999;
    for ix = 1 : size(im,2)
        for iy = 1 : size(im,1)
            if ~isequal([ix,iy],[x,y]) && asteroidMap(iy,ix) == 1
                if ix == x && iy < y
                    a = 0;
                elseif ix == x && iy > y
                    a = 180;
                elseif iy == y && ix > x
                    a = 90;
                elseif iy == x && ix < x
                    a = 270;
                elseif ix >= x && iy <= y
                    a = atand((ix-x)/(y-iy));
                elseif ix >= x && iy >= y
                    a = 180 - atand((ix-x)/(iy-y));
                elseif ix <= x && iy >= y
                    a = 180 + atand((x-ix)/(iy-y));
                elseif ix <= x && iy <= y
                    a = 360 - atand((x-ix)/(y-iy));
                end
                angle(iy,ix) = a;
            end
        end
    end
    angleCopy = angle;
    for idx = 1 : nnz(asteroidMap)
        %Get the index of the next asteroid to vaporize
        [yVap, xVap] = ind2sub(size(im), find(angleCopy == min(angleCopy(:))));
        numVaporizedSoFar = numVaporizedSoFar + 1;
        angleCopy(yVap, xVap) = 999;
        vaporized(yVap, xVap) = numVaporizedSoFar;
        im(yVap, xVap) = '.';
    end
end

