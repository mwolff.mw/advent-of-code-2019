


% signal = '12345678';
% for idx = 1 : 4
%     signal = fft(signal)
% end
% 
% signal = '80871224585914546619083218645595';
% for idx =  1 : 100
%     signal = fft(signal);
% end
% signal(1:8)

signal = '59758034323742284979562302567188059299994912382665665642838883745982029056376663436508823581366924333715600017551568562558429576180672045533950505975691099771937719816036746551442321193912312169741318691856211013074397344457854784758130321667776862471401531789634126843370279186945621597012426944937230330233464053506510141241904155782847336539673866875764558260690223994721394144728780319578298145328345914839568238002359693873874318334948461885586664697152894541318898569630928429305464745641599948619110150923544454316910363268172732923554361048379061622935009089396894630658539536284162963303290768551107950942989042863293547237058600513191659935';
% for idx =  1 : 100
%     signal = fft(signal);
% end
% signal(1:8)

% signal = '03036732577212944063491565474664';
% offset = 303673;
% signal = repmat(signal, 1, 1e4);
% for idx =  1 : 100
%     signal = fft_fast(signal, offset);
% end
% signal(offset+1:offset+8)
% 
% signal = '02935109699940807407585447034323';
% offset = 293510;
% signal = repmat(signal, 1, 1e4);
% for idx =  1 : 100
%     signal = fft_fast(signal, offset);
% end
% signal(offset+1:offset+8)
% 
% signal = '03081770884921959731165446850517';
% offset = 308177;
% signal = repmat(signal, 1, 1e4);
% for idx =  1 : 100
%     signal = fft_fast(signal, offset);
% end
% signal(offset+1:offset+8)

signal = '59758034323742284979562302567188059299994912382665665642838883745982029056376663436508823581366924333715600017551568562558429576180672045533950505975691099771937719816036746551442321193912312169741318691856211013074397344457854784758130321667776862471401531789634126843370279186945621597012426944937230330233464053506510141241904155782847336539673866875764558260690223994721394144728780319578298145328345914839568238002359693873874318334948461885586664697152894541318898569630928429305464745641599948619110150923544454316910363268172732923554361048379061622935009089396894630658539536284162963303290768551107950942989042863293547237058600513191659935';
offset = 5975803;
signal = repmat(signal, 1, 1e4);
for idx =  1 : 100
    tic
    idx,
    signal = fft_fast(signal, offset);
    toc
end
signal(offset+1:offset+8)

% signal = repmat(signal, 1, 1e4);
% for idx =  1 : 100
%     tic
%     signal = fft_fast(signal, 5975803);
%     toc
% end
% signal(1:8)

% indicesIn = 5975803 + (1:8);
% requiredIndices = cell(1,100);
% for idx = 1 : 100
%     tic
%     requiredIndices{idx} = getRequiredIndices(numel(signal), indicesIn);
%     indicesIn = requiredIndices{idx};
%     toc
% end



% signal = '59758034323742284979562302567188059299994912382665665642838883745982029056376663436508823581366924333715600017551568562558429576180672045533950505975691099771937719816036746551442321193912312169741318691856211013074397344457854784758130321667776862471401531789634126843370279186945621597012426944937230330233464053506510141241904155782847336539673866875764558260690223994721394144728780319578298145328345914839568238002359693873874318334948461885586664697152894541318898569630928429305464745641599948619110150923544454316910363268172732923554361048379061622935009089396894630658539536284162963303290768551107950942989042863293547237058600513191659935';
% signal = repmat(signal, 1, 1e4);
% for idx =  1 : 100
%     tic
%     signal = fft(signal);
%     toc
% end

function indicesOut = getRequiredIndices(signalLength, indicesIn)
pattern = zeros(1,signalLength);
for idx = 1 : numel(indicesIn)
    tic
    n = indicesIn(idx);
    patternTmp = [zeros(1,n), ones(1,n), zeros(1,n), -ones(1,n)];
    patternTmp = repmat(patternTmp, 1, ceil(signalLength/numel(patternTmp))+1);
    patternTmp = patternTmp(2:signalLength+1);
    pattern = pattern + abs(patternTmp);
    toc
end
indicesOut = 1 : signalLength;
indicesOut = indicesOut(pattern ~= 0);
end


function signalOut = fft(signalIn, idxStart)
if nargin == 1; idxStart = 1; end
length = numel(signalIn);
%
signalIn = signalIn(idxStart:end);
%Convert the signal to an array of numbers
signalInNum = double(signalIn)-double('0');
signalOut = repmat('0',1,length);
for idx = idxStart : length
    tic
    pattern = [ones(1,idx), zeros(1,idx), -ones(1,idx), zeros(1,idx)];
    toc
    tic
    pattern = repmat(pattern, 1, ceil(numel(signalIn)/numel(pattern))+1);
    toc
    tic
    pattern = pattern(1:numel(signalIn));
    toc
    tic
    result = num2str(sum(signalInNum .* pattern));
    toc
    signalOut(idx) = result(end);
    toc
end
end    

function signalOut = fft_fast(signalIn, idxStart)
if nargin == 1; idxStart = 1; end
length = numel(signalIn);
%
signalIn = signalIn(idxStart:end);
%Convert the signal to an array of numbers
signalInNum = double(signalIn)-double('0');
signalOut = repmat('0',1,length);
result = sum(signalInNum);
signalOut(idxStart) = num2str(result - floor(result*0.1)*10);
for idx = idxStart+1 : length
    result = result - signalInNum(idx-idxStart);
%     signalInNum(1 : idx-idxStart) = 0;
%     result = num2str(sum(signalInNum)-sum(signalInNum(1 : idx-idxStart));
    signalOut(idx) = num2str(result - floor(result*0.1)*10);
end
end    
    
 