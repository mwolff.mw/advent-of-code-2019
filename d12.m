moons = {'Io', 'Europa', 'Ganymede', 'Callisto'};

posInput = {'x=-1, y=0, z=2', 'x=2, y=-10, z=-7', 'x=4, y=-8, z=8', 'x=3, y=5, z=-1'};
% posInput = {'x=-8, y=-10, z=0', 'x=5, y=5, z=10', 'x=2, y=-7, z=3', 'x=9, y=-8, z=-3'};
posInput = {'x=-10, y=-13, z=7', 'x=1, y=2, z=1', 'x=-15, y=-3, z=13', 'x=3, y=7, z=-4'};

%Initialize positions
pos = [...
    cellfun(@(x) str2double(strrep(regexp(x, 'x=[-]?[0-9]+', 'match'), 'x=', '')), posInput); ...
    cellfun(@(x) str2double(strrep(regexp(x, 'y=[-]?[0-9]+', 'match'), 'y=', '')), posInput); ...
    cellfun(@(x) str2double(strrep(regexp(x, 'z=[-]?[0-9]+', 'match'), 'z=', '')), posInput)];
posStart = pos;

%Initialize velocity to zero
vel = zeros(3,4);
velStart = vel;

numSteps = 1e6;
energy = zeros(1,numSteps);
var = zeros(3,numSteps);
for step = 1:numSteps
    for ii = 1:numel(moons)-1
        for jj = ii+1:numel(moons)
            [vel(1,ii), vel(1,jj)] = gravity(pos(1,ii), pos(1,jj), vel(1,ii), vel(1,jj));
            [vel(2,ii), vel(2,jj)] = gravity(pos(2,ii), pos(2,jj), vel(2,ii), vel(2,jj));
            [vel(3,ii), vel(3,jj)] = gravity(pos(3,ii), pos(3,jj), vel(3,ii), vel(3,jj));
        end
    end
    pos = pos + vel;
    var(1,step) = max((pos(1,:)));
    var(2,step) = max((pos(2,:)));
    var(3,step) = max((pos(3,:)));
end
disp('start looking for period on x');
for idx = 1 : 2e5; if isequal(var(1,1:idx), var(1,idx+1:idx+idx)), idx, break; end; end
disp('start looking for period on y');
for idx = 1 : 2e5; if isequal(var(2,1:idx), var(2,idx+1:idx+idx)), idx, break; end; end
disp('start looking for period on z');
for idx = 1 : 2e5; if isequal(var(3,1:idx), var(3,idx+1:idx+idx)), idx, break; end; end

energy(step) = sum(arrayfun(@(x) totalEnergy(pos(:,x), vel(:,x)), 1:4));
sum(energy)


% 
% step = 1;
% pos = posStart;
% vel = velStart;
% doComputations = true;
% while doComputations
%     for ii = 1:numel(moons)-1
%         for jj = ii+1:numel(moons)
%             [vel(1,ii), vel(1,jj)] = gravity(pos(1,ii), pos(1,jj), vel(1,ii), vel(1,jj));
%             [vel(2,ii), vel(2,jj)] = gravity(pos(2,ii), pos(2,jj), vel(2,ii), vel(2,jj));
%             [vel(3,ii), vel(3,jj)] = gravity(pos(3,ii), pos(3,jj), vel(3,ii), vel(3,jj));
%         end
%     end
%     pos = pos + vel;
%     doComputations = ~(isequal(pos, posStart) && isequal(vel, velStart));
%     step = step + 1;
%     if mod(step, 1e5) == 0; step, end
% end
% 
% step-1

function [v1,v2] = gravity(p1,p2,v1,v2)
if p1 < p2
    v1 = v1+1;
    v2 = v2-1;
elseif p1 > p2
    v1 = v1-1;
    v2 = v2+1;
end
end

function energy = totalEnergy(pos, vel)
pot = sum(abs(pos));
kin = sum(abs(vel));
energy = pot * kin;
end