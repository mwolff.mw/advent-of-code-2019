function [out, asteroidMap] = d10_numAsteroids(im, x, y)
%D10_NUMASTEROIDS Summary of this function goes here
%   Detailed explanation goes here

asteroidMap = zeros(size(im));
for ix = 1 : size(im,2)
    for iy = 1 : size(im,1)
        %Only consider locations that are different from the input location
        %and on which there is an asteroid
        if ~isequal([x,y],[ix,iy]) && im(iy,ix) == '#'
            if (abs(ix-x) == 1 && abs(iy-y) == 0) || (abs(ix-x) == 0 && abs(iy-y) == 1) || (abs(ix-x) == 1 && abs(iy-y) == 1)
                asteroidMap(iy,ix) = 1;
            elseif ix == x
                %Vertical path
                asteroidMap(iy,ix) = double(all(im(min(iy,y)+1 : max(iy,y)-1, x) == '.'));
            elseif iy == y
                %Horizontal path
                asteroidMap(iy,ix) = double(all(im(y, min(ix,x)+1 : max(ix,x)-1) == '.'));
            else
                %Compute slope
                directLineOfSight = true;
                for jx = min(ix,x) : max(ix,x)
                    for jy = min(iy,y) : max(iy,y)
                        if ~isequal([jx,jy],[x,y]) && ~isequal([jx,jy],[ix,iy]) && im(jy,jx) == '#' && directLineOfSight
                            %Compute slopes with both objects
                            s1 = (jy-iy) / (jx-ix);
                            s2 = (jy-y) / (jx-x);
                            directLineOfSight = abs(abs(s1)-abs(s2)) > 1e-7;
                        end
                    end
                end
                if directLineOfSight
                    asteroidMap(iy,ix) = 1;
                end
            end
        end
    end
end
out = nnz(asteroidMap);