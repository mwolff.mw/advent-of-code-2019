classdef Communicator < handle
    %FILEBASEDCOMMUNICATOR Implementation of a communicator that uses text
    %files as communication protocol
    
    properties (Access = private)
        location;
        filename;
        lastSize;
    end
    
    methods (Access = private)
        
        function lastSize = getLastSize(obj)
            lastSize = getfield(dir(obj.filename), 'bytes');
        end
        
        function data = readLastLine(obj)
            if ispc
                [returnCode, data] = system(['powershell.exe Get-Content "', obj.filename, '" -Tail 1']);
                % Remove the EOF character
                data = data(1:end-1);
            else
                % TODO
            end
        end
        
        
    end
    
    methods
        function obj = Communicator(filename, flagCreateFile)
            %FILEBASEDCOMMUNICATOR Construct an instance of this class
            obj.filename = filename;
            [obj.location, ~, ~] = fileparts(obj.filename);
            if nargin == 2 && flagCreateFile; fclose(fopen(obj.filename, 'w+')); end
            obj.lastSize = obj.getLastSize();
        end
        
        function data = read(obj)
            %READ Read new data from the communication file
            lastSizeActual = obj.getLastSize();
            if lastSizeActual > obj.lastSize
                data = obj.readLastLine();
                obj.lastSize = lastSizeActual;
                %disp(['Read data: ', data]);
                data = jsondecode(data);
            else
                data = [];
            end
        end
        
        function data = getNextData(obj)
            data = [];
            %disp('Waiting for data...');
            while isempty(data)
                data = obj.read();
            end
        end
        
        function write(obj, data)
            f = fopen(obj.filename, 'a');
            data = jsonencode(data);
            %disp(['Writing data: ', data]);
            fprintf(f, '%s\n', data);
            fclose(f);
        end
    end
end

